package com.example.zveri.telemedicine.core_fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.zveri.telemedicine.R;

/**
 * Created by OlgaA on 16.12.2017.
 */

public class ApprovedRequestFragment extends Fragment {

    private String prevTitle;

    @Override
    public void onStart() {
        super.onStart();
        prevTitle = getActivity().getTitle().toString();
        getActivity().setTitle("Notification");
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().setTitle(prevTitle);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_approved_request, null);
    }

}
