package com.example.zveri.telemedicine.core_fragments;

import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.zveri.telemedicine.R;

public class DoctorDetailsFragment extends Fragment {

    private String prevTitle;

    @Override
    public void onStart() {
        super.onStart();
        prevTitle = getActivity().getTitle().toString();
        getActivity().setTitle("Doctor list");
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().setTitle(prevTitle);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_doctor_details, null);
    }

}
