package com.example.zveri.telemedicine.core_fragments;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.util.Log;
import android.view.MenuItem;

import com.example.zveri.telemedicine.R;

import java.lang.reflect.Field;

/**
 * Created by deDwarf on 1/8/2018.
 */

public class NavigationUtils {

    private Activity context;

    private NavigationUtils(Activity context){
        this.context = context;
    }

    public static void initializeBottomNavBar(BottomNavigationView navigation){
        NavigationUtils nu = new NavigationUtils((Activity) navigation.getContext());
        navigation.setOnNavigationItemSelectedListener(nu.mOnNavigationItemSelectedListener);
        disableShiftMode(navigation);
    }

    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentTransaction ft = context.getFragmentManager().beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    ft.replace(R.id.frgmCont, new HomeFragment());
                    ft.commit();
                    return true;
                case R.id.navigation_notif:
                    ft.replace(R.id.frgmCont, new ApprovedRequestFragment());
                    ft.commit();
                    return true;
                case R.id.navigation_schedule:
                    ft.replace(R.id.frgmCont, new DoctorListFragment());
                    ft.commit();
                    return true;
                case R.id.navigation_profile:
                    ft.replace(R.id.frgmCont, new DoctorDetailsFragment());
                    ft.commit();
                    return true;
            }
            return false;
        }

    };

    private static void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                //noinspection RestrictedApi
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                //noinspection RestrictedApi
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        }
    }

}
