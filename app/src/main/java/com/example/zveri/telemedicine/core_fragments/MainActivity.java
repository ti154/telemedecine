package com.example.zveri.telemedicine.core_fragments;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;

import com.example.zveri.telemedicine.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView nav = (BottomNavigationView) findViewById(R.id.navigation);
        NavigationUtils.initializeBottomNavBar(nav);

        FragmentTransaction t = getFragmentManager().beginTransaction();
        t.add(R.id.frgmCont, new HomeFragment());
        t.commit();
    }


}
